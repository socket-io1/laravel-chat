<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class chatlog extends Model
{
    //
    protected $table = "chatlog";
	protected $primaryKey = 'idchat';
	public $incrementing = true;

    protected $guarded = [
        'created_at', 'updated_at',
    ];

    protected $fillable = [
        'idchat',
        'name',
        'roomId',
        'message',
    ];

    function create($array){
        chatlog::insert($array);
    }

    function getData($id){
        return chatlog::where('roomId',$id)->get();
    }
}
