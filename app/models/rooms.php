<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class rooms extends Model
{
    //
    protected $table = "rooms";
	protected $primaryKey = 'roomId';
	public $incrementing = true;

    protected $guarded = [
        'created_at', 'updated_at',
    ];

    protected $fillable = [
        'roomId',
        'roomType',
        'roomUser1',
        'roomUser2'

    ];

    function checkUser1($id){
        return rooms::where('roomUser1',$id)
        ->where('roomUser2',Auth::User()->id)->first();
    }

    function checkUser2($id){
        return rooms::where('roomUser2',$id)
        ->where('roomUser1',Auth::User()->id)->first();
    }

    function checkAuthority($id){
        $user=Auth::User()->id;
        return rooms::where('roomId',$id)
        ->where(function($query) use ($user) {
            $query->where('roomUser1', $user)
                  ->orWhere('roomUser2', $user);
          })
        ->first();
    }

    function createRoom($id){
        $data = new rooms;
        $data->roomUser1=$id;
        $data->roomUser2=Auth::User()->id;
        if($data->save()){
            return $data->roomId;
        }
    }
}
