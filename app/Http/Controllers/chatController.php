<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\models\rooms;
use App\models\chatlog;
use Auth;

class chatController extends Controller
{
    //
    function group(){
        $chatlog = new chatlog;
        $data = $chatlog->getData(0);
        return view('chat.group',[
            'data'=>$data
        ]);
    }

    function personal($room){
        $rooms = new rooms;
        $data1=$rooms->checkAuthority($room);
        // dd($data1);
        if(!empty($data1)){
            $chatlog = new chatlog;
            $data = $chatlog->getData($room);
            return view('chat.personal',[
                'room'=>$room,
                'data'=>$data
            ]);
        }else{
            return redirect()->route('home')->with('errors!','Ops anda tidak berhak atas page ini');   
        }
       
    }

    function generate(Request $request){
        $user = new User;
        $id=$request->iduser;
        $cekuser=$user->checkUser($id);

        if(Auth::User()->id==$id){
            return redirect()->route('home')->with('errors!','Tidak boleh pakai id sendiri'); 
        }else if(!empty($cekuser)){
            try {
                //code...
                $checkRoom =  $this->checkRoom($id);
                return redirect()->route('chat.personal',['room'=>$checkRoom]);
            } catch (\Exception $th) {
                throw $th;
                return redirect()->route('home')->with('errors!','Ops Ada error');
            }
        }else{
            return redirect()->route('home')->with('errors!','Wah usernya g ketemu nih');
        }
    }

    function checkRoom($id){
        $rooms = new rooms;
        $data1=$rooms->checkUser1($id);
        $data2=$rooms->checkUser2($id);
        // dd($id,$data1,$data2);
        if($data1!==null){
            return $data1->roomId;
        }if($data2!==null){
            return $data2->roomId;
        }else{
            $data=$rooms->createRoom($id);
            return $data;
        }
    }

    function createLog(Request $request){
        $this::validate($request,[
            'name' => '|required|max:255',
            'roomId' => 'numeric|required|max:255',
            'message' => '|required|max:500'
        ]);

        try {
            //code...
            $array=[
                'name'=>$request->name,
                'roomId'=>$request->roomId,
                'message'=>$request->message,
                'created_at'=>date('Y-m-d-H-i-s'),
                'updated_at'=>date('Y-m-d-H-i-s')
            ];
            $chatlog= new chatlog;
            $chatlog->create($array);
            return 'ok';
        } catch (\Exception $th) {
            throw $th;
            return 'error';
        }
    }
}
