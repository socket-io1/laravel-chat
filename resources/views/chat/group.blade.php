<!doctype html>
<html>
  <head>
    <title>Socket.IO chat</title>
    <style>
      * { margin: 0; padding: 0; box-sizing: border-box; }
      body { font: 13px Helvetica, Arial; }
      form { background: #000; padding: 3px; position: fixed; bottom: 0; width: 100%; }
      form input { border: 0; padding: 10px; width: 90%; margin-right: 0.5%; }
      form button { width: 9%; background: rgb(130, 224, 255); border: none; padding: 10px; }
      #messages { list-style-type: none; margin: 0; padding: 0; }
      #messages li { padding: 5px 10px; }
      #messages li:nth-child(odd) { background: #eee; }
	  
		.left-party{
			background:red!important;
			color:white;
		}
		.join-party{
			background:blue!important;
			color:white;
		}
    </style>
  </head>
  <body>
    <ul id="messages">
        @foreach ($data as $d )
            <li>
            {{$d->name}}: {{$d->message}}
            </li>
        @endforeach
    </ul>
    <form action="">
      <input id="m" autocomplete="off" /><button>Send</button>
    </form>
  </body>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
	<script src="/js/socket.io.js"></script>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script>
		let roomnumber = getSearchParameters(2)
		let numberofroom = 'group';
        let usernames = '{{ Auth::User()->name}}';
        function createLog(name,room,variable){
           axios.post('{{ route('createlog') }}', {
                    name : name,
                    roomId : room,
                    message : variable
                })
                .then(function (response) {
                    console.log(response);
                    
                })
                .catch(function (error) {
                    console.log(error);
                }); 
        }
		$(function () {
			//var socket = io();
			
			// var socket = io();
			// socket.on('connectToRoom',function(data) {
			// 	// document.body.innerHTML = '';
			// 	// document.write(data);
			// 	$('#messages').append($('<li>').text(data));
			// 	window.scrollTo(0, document.body.scrollHeight);
			// });
			
			var socket = io.connect('{{env('APP_OPENSOCKET_LINK')}}');
			socket.emit('join', numberofroom);
			
			$('form').submit(function(e) {
                
                let variable = $('#m').val();
                e.preventDefault(); // prevents page reloading
                socket.emit('new message', variable);
                $('#m').val('');
                createLog(usernames,0,variable)
                
                return false;
			});
			
			socket.on('new_msg', function(data){
				console.log('weweweww'+data)
			});

			socket.on("new_msg", function(data) {
				alert(data);
			});

			socket.on('new message', function(data){
			$('#messages').append($('<li>').text(data.username+': '+data.message));
			window.scrollTo(0, document.body.scrollHeight);
			});
			socket.on('user joined', function(msg){
			$('#messages').append($('<li class="join-party">').text(''+msg.username+' connect'));
			window.scrollTo(0, document.body.scrollHeight);
			});
			socket.on('user left', function(msg){
			$('#messages').append($('<li class="left-party">').text(''+msg.username+' disconnect'));
			window.scrollTo(0, document.body.scrollHeight);
			});
			var params = getSearchParameters(1);
			console.log(params.name)
             
			socket.emit('add user', usernames);
			
			
			
		});

		function generateJson(data){
			return {
				data: data,
				room: numberofroom
			}
		}
		
		function getSearchParameters(nums) {
			var prmstr = window.location.search.substr(nums);
			return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
		}

		function transformToAssocArray( prmstr ) {
			var params = {};
			var prmarr = prmstr.split("&");
			for ( var i = 0; i < prmarr.length; i++) {
				var tmparr = prmarr[i].split("=");
				params[tmparr[0]] = tmparr[1];
			}
			return params;
		}
	</script>
</html>