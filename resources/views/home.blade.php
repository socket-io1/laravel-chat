@extends('layouts.app')

@section('content')

@if(session()->has('errors!'))
    <span>
    
    </span>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background: red;color: white;">{{ session('errors!') }}</div>
                </div>
            </div>
        </div>
    </div>
@endif 
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Halo, {{ Auth::User()->name }}</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <br>
                    <br>
                    <div class="col-md-12 ">
                        <form method="post"  action="{{ route('chat.generate') }}" class="form-horizontal">

                            {{csrf_field()}}
                            {{-- <div class="col-md-1">
                              
                            </div> --}}
                            <div class="form-group">
                                <label for="iduser" class="col-md-4 control-label">Pilih User Yang Akan Di Chat</label>
                                <div class="col-md-4">
                                    <select name=iduser class="form-control">
                                        @foreach ($datauser as $data )
                                            <option value="{{ $data->id }}">{{ $data->name }}</option>
                                        @endforeach
                                        
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Chat
                                    </button>
                                </div>
                            </div>
                            
                            
                        </form>
                    </div> 
                    <div class="col-md-12 ">
                        <center>
                            atau
                        </center> 
                    </div>   
                    <div class="col-md-12 ">
                        <center>
                            <a href="{{ route('group') }}" type="submit" class="btn btn-primary">
                                Klik disini untuk chat group
                            </a>
                        </center>   

                    </div> 
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
