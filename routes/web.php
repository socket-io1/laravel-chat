<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function () {

    route::get('/group',[
        'uses' => 'chatController@group',
        'as' => 'group'
    ]);

    route::get('/personal/{room}/chat',[
        'uses' => 'chatController@personal',
        'as' => 'chat.personal'
    ]);
    
    route::post('/personal/generate',[
        'uses' => 'chatController@generate',
        'as' => 'chat.generate'
    ]);

});